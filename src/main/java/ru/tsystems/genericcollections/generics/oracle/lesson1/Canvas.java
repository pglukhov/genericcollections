package ru.tsystems.genericcollections.generics.oracle.lesson1;

import java.util.LinkedList;
import java.util.List;

public class Canvas {
    public void draw(Shape s) {
        s.draw(this);
    }

    public void drawAll(List<? extends Shape> shapes) {
        for (Shape s: shapes) {
            s.draw(this);
        }
    }

    public static void main(String[] args) {
        Canvas canvas = new Canvas();
        Shape myShape = new Shape() {
            @Override
            public void draw(Canvas c) {

            }
        };

        List<Shape> myList = new LinkedList<>();
        canvas.drawAll(myList);
        canvas.draw(myShape);
    }

}
