package ru.tsystems.genericcollections.generics.oracle.lesson1;

import java.util.ArrayList;
import java.util.Collection;

public class TestForLesson1 {
    static  <T> void fromArrayToCollection(T[] a, Collection<T> c) {
        for (T o : a) {
            c.add(o); // Correct
        }
    }


    public static void main(String[] args) {
        Object[] oa = new Object[100];
        Collection<Object> co = new ArrayList<Object>();

// T inferred to be Object
        fromArrayToCollection(oa, co);

    }
}
