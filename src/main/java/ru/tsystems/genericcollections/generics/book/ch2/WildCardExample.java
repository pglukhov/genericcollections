package ru.tsystems.genericcollections.generics.book.ch2;

import java.util.*;

public class WildCardExample {
    public static double sum(Collection<? extends Number> nums) {
        double s = 0.0;
        for (Number num : nums) {
            s += num.doubleValue();
        }

        return s;
    }

    public static void count(Collection<? super Integer> ints, int n) {
        for (int i = 0; i < n; i++) {
            ints.add(i);
        }
    }

    public static void main(String[] args) {
        List<Integer> ints = new ArrayList<>();
        ints.add(1);
        ints.add(2);
        List<? extends Number> nums = ints;
        // compile-time error
        // nums.add(3.14);

        List<Object> objs = Arrays.asList(2, 3.14, "four");
        List<Integer> integers = Arrays.asList(5, 6);
        Collections.copy(objs, integers);
        assert objs.toString().equals("[5, 6, four]");

        System.out.println(sum(Arrays.asList(2, 3, 4, 5, 5.5, 0.5)));


        List<Integer> ints2 = new ArrayList<>();
        count(ints2, 10);

        List<Number> nums2 = new ArrayList<>();
        count(nums2, 4);
        nums2.add(5.5);
        System.out.println(nums2);

        WildCardExample wildCardExample = new WildCardExample();
        System.out.println(wildCardExample.hashCode());
        System.out.println((new WildCardExample()).hashCode());
        System.out.println((new WildCardExample()).hashCode());
    }
}
