package ru.tsystems.genericcollections.generics.book.ch1;


import java.util.LinkedList;
import java.util.List;

public class GenericsTypeErasure {
    public static void main(String[] args) {
        List<String> lStr = new LinkedList<>();
        List<Integer> lInt = new LinkedList<>();
        List<List<Integer>> ll = new LinkedList<>();
        int[] arr = new int [10];
        System.out.println(lStr.getClass().getName());
        System.out.println(lInt.getClass().getName());
        System.out.println(ll.getClass().getName());
        System.out.println(ll.getClass().getCanonicalName());
        System.out.println(arr.getClass().getName());
        System.out.println(arr.getClass().getCanonicalName());

    }
}
