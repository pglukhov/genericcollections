package ru.tsystems.genericcollections.generics.book.ch1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SumOfElements {
    public static void sumWithGenerics() {
        List<Integer> ints = Arrays.asList(1,2,3);
        int s = 0;
        for(int n:ints) {
            s+=n;
        }

        assert s==6;
    }

    public static void sumWithoutGenerics() {
        List<Integer> ints = Arrays.asList(new Integer (1), new Integer (2),new Integer (3));
        int s = 0;
        for(Iterator it = ints. iterator(); it.hasNext();) {
            int n = ((Integer)it.next()).intValue();
            s+=n;

        }
        assert s==6;
    }

    public static void main(String[] args) {
        sumWithGenerics();
        sumWithoutGenerics();
    }

}
