package ru.tsystems.genericcollections.generics.book.ch1;

import java.util.List;

public class DeclareList {
    public static void main(String[] args) {
        List<Integer> ints = Lists.<Integer>toList();
        List<Object> objs = Lists.<Object>toList(1, "two");
    }
}
