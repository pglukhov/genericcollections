package ru.tsystems.genericcollections.generics.book.ch1;


import java.util.ArrayList;
import java.util.List;

public class Lists {
    public static <T> List<T> toList(T...arr) {
        List<T> list = new ArrayList<>();
        for(T elt:arr) {
            list.add(elt);

        }
        return list;
    }
}
