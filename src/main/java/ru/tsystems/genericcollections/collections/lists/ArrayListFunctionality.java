package ru.tsystems.genericcollections.collections.lists;

import java.util.ArrayList;
import java.util.List;

public class ArrayListFunctionality {
    public static void main(String[] args) {
        List<Integer> lists = new ArrayList<>();
        lists.add(null);
        lists.add(null);
        lists.add(null);
        lists.add(null);
        lists.add(null);

        System.out.println(lists.size());
    }
}
