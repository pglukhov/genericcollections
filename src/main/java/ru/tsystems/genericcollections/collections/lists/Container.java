package ru.tsystems.genericcollections.collections.lists;

public interface Container<T> {
    public Iterator<T> getIterator();
}