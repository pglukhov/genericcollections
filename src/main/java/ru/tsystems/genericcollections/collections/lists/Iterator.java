package ru.tsystems.genericcollections.collections.lists;

public interface Iterator<T> {
    public boolean hasNext();

    public T next();
}