package ru.tsystems.genericcollections.collections.lists;


public class LinkListMy<T> implements Container<T> {
    private Entity<T> header;

    @Override
    public Iterator<T> getIterator() {
        return new LinkListIterator();
    }

    private class LinkListIterator implements Iterator<T> {
        private Entity<T> current = header.next;

        @Override
        public boolean hasNext() {
            return current.next != header.next;
        }

        @Override
        public T next() {
            Entity<T> entity = current;
            current = current.next;
            return (T) entity.value;
        }
    }

    private static class Entity<T> {
        private Entity<T> prev;
        private Entity<T> next;
        private T value;

        public Entity() {
        }

        public Entity(Entity prev, Entity next, T value) {
            this.prev = prev;
            this.next = next;
            this.value = value;
        }

        public Entity<T> getPrev() {
            return prev;
        }

        public void setPrev(Entity<T> prev) {
            this.prev = prev;
        }

        public Entity<T>  getNext() {
            return next;
        }

        public void setNext(Entity<T>  next) {
            this.next = next;
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }
    }

    public LinkListMy() {
        header = new Entity<T>();
        header.next = header;
        header.prev = header;
    }

    public void add(T value) {
        // link to the last element
        Entity<T> lastEntity = header.prev;

        Entity<T> entity = new Entity<>();
        entity.value = value;
        entity.next = header;
        entity.prev = lastEntity;
        lastEntity.next = entity;
        header.prev = entity;
    }

    public static void main(String[] args) {
        LinkListMy<Integer> linkListMy = new LinkListMy<>();
        linkListMy.add(1);
        linkListMy.add(2);
        linkListMy.add(3);

        Entity<Integer> current = linkListMy.header.next;
        while (current.next != linkListMy.header.next) {
            System.out.println(current.value);
            current = current.next;
        }
        Iterator<Integer> iterator = linkListMy.getIterator();

        System.out.println("Iterator print");
        while (iterator.hasNext()) {
            Integer value = iterator.next();
            System.out.println(value);
        }
    }
}
