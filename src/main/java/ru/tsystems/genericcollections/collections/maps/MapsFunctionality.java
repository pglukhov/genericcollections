package ru.tsystems.genericcollections.collections.maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapsFunctionality {
    public static void main(String[] args) {
        Map<Integer, String> hashmap = new HashMap<>();
        for (int i=0; i<10; i++) {
            hashmap.put(i, String.valueOf(i));
        }

        // 1.
        for (Map.Entry<Integer, String> entry: hashmap.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }

        // 2.
        for (Integer key: hashmap.keySet()) {
            System.out.println(hashmap.get(key));
        }

        // 3.
        Iterator<Map.Entry<Integer, String>> itr = hashmap.entrySet().iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}
