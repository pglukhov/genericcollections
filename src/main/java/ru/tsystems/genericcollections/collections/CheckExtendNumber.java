package ru.tsystems.genericcollections.collections;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CheckExtendNumber {
    public static void main(String[] args) {
        List<? extends Number> myClass = someList();
        System.out.println(myClass);

        List<Object> objs = Arrays.<Object>asList(2, 3.14, "four");
        List<Integer> ints = Arrays.asList(5, 6);
        Collections.copy(objs, ints);
        assert objs.toString().equals("[5, 6, four]");
    }

    static List<Integer> someList() {
        return Arrays.<Integer>asList(1, 2, 3, 4, 5);
    }
}
