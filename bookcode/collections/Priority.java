package ru.tsystems.genericcollections.collections;

public enum Priority { HIGH, MEDIUM, LOW }