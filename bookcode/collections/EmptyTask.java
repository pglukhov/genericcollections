package ru.tsystems.genericcollections.collections;

public class EmptyTask extends Task {
  public EmptyTask() {}
  public String toString() { return ""; }
}
